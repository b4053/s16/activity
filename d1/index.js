console.log("Hello World"); 

//<--------------------------------------LOOPS

//while loop

/*
syntax:
	while(expression/condition){
		statement
	}

	(while - how many loop
		if condition is true - statement will execute)

*/

let count = 10;

while (count !== 0) {
	console.log("Don't text her back.")
	count--;
}

let count1 = 5;

while (count1 !== 0) {
	console.log("count1")
	count1--;
}


// Miniactivity

let count2 = 1;
while (count2 <= 6) {
	console.log("count2")
	count2++;
}



//do while loop
/*run atleast once then itirate

syntax:
	do {
		statement ---> first to run
	} while (expressio/condition)

*/

let doWhileCounter = 1;
do {
	console.log(doWhileCounter)
	doWhileCounter++
}while (doWhileCounter <= 20)



let number = Number(prompt("Give me a number"))
do{
	console.log("Do while: " + number);
	number +=1;
} while(number<10);


//for loop
/*3 parts
	1. initialization - value that will tract the progression of the loop
	2. expression/condition - decide to run again or not
	3. final expression - how to advance the loop(++,--)


syntax:
	for(initialization; expression/condition; final expression){
	statement
	}

*/

// loop from 0-20
for (let count = 0; count <=20; count ++){
	console.log(count);
}

// for loops accessing array items

let fruits = ["Apple", "Durian", "Kiwi", "Pineapple", "Orange"]

console.log(fruits[2]);
console.log(fruits.length); //total number of items in the array
console.log(fruits[fruits.length-1]); //-1 maaccess ang pinakalast na item

for(let index = 0; index < fruits.length; index++){
	console.log(fruits[index])
}


//Miniactivity

let country = ["France", "Norway", "Canada", "Iceland", "Singapore", "USA"]

for(let index = 0; index < country.length-1; index++){
	console.log(country[index])
}


//for loops accessing elements of a string - indexing or length
//.length is also a property used in strings

let myString = "alex";
console.log(myString.length);
console.log(myString[0])

//loop for individual character of mystreing variable

for (let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}


let myName = "Jane";

for (let i = 0; i < myName.length; i++){
	// console.log(myName[i]);
	if(myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	){
		console.log(3)
	} else {
		console.log(myName[i])
	}
}


//continue and break statements


for(let count = 0; count <= 20; count++){
	if(count % 2 === 0){
		continue;
	} 
	console.log("Continue and Break:" + count);
	if(count > 10){
		break;
	}
}

