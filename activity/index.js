console.log("Hello World"); 

//<--------------------------- s16 ACTIVITY --------------------------->


//3. Create a variable number that will store the value of the number provided by the user via the prompt.
//4. Create a for loop that will be initialized with the number provided by the user, 
	//will stop when the value is less than or equal to 0 and 
	//will decrease by 1 every iteration.
//5. Create a condition that if the current value is less than or equal to 50, stop the loop.
//6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
//7. Create another condition that if the current value is divisible by 5, print the number.

let userInput = Number(prompt("Please enter a number."));
console.log(`The number you provided is ${userInput}.`);

for (let curVal = userInput; curVal >= 0; curVal -- ){
	if (curVal % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	} if (curVal % 5 === 0){
		console.log(curVal)
	}
	if (curVal <= 51){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}
}


//8. Create a variable that will contain the string supercalifragilisticexpialidocious.
//9. Create another variable that will store the consonants from the string.

let strName = "supercalifragilisticexpialidocious";
console.log(strName)
let strConso = "sprclfrglstcxpldcs"
console.log(strConso)


//10. Create another for Loop that will iterate through the individual letters of the string based on it’s length.

for(let neym = 0; neym < strName.length; neym ++) {
	console.log(strName[neym]);

//11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

	if(
		strName[neym].toLowerCase() == "a" ||
		strName[neym].toLowerCase() == "e" ||
		strName[neym].toLowerCase() == "i" ||
		strName[neym].toLowerCase() == "o" ||
		strName[neym].toLowerCase() == "u" 
		) {
		console.log("Vowel checked. Continue.");
		continue;
	// } else {
	// 	console.log(`${strConso[0]}u${strConso[1]}e${strConso[2]}${strConso[3]}a${strConso[4]}i${strConso[5]}${strConso[6]}a${strConso[7]}i${strConso[8]}i${strConso[9]}${strConso[10]}i${strConso[11]}e${strConso[12]}${strConso[13]}ia${strConso[14]}i${strConso[15]}o${strConso[16]}iou${strConso[17]}`)
	}
}


//12. Create an else statement that will add the letter to the second variable.